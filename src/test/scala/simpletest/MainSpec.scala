package simpletest

import org.scalatest.funspec.AnyFunSpec

class MainSpec extends AnyFunSpec {

  describe("Main::run") {
    it("should return fizz when 3 divides the given number") {
      assert(Main.process(6) == "fizz")
    }

    it("should return the number itself when neither 3 nor 5 divides it") {
      assert(Main.process(7) == "7")
    }

    it("should return buzz when 5 divides the given number") {
      assert(Main.process(20) == "buzz")
    }

    it("should return fizzbuzz when 15 divides the given number") {
      assert(Main.process(0) == "fizzbuzz")
    }

  }
}
