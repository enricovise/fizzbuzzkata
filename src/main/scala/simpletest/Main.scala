package simpletest

object Main {
  def process(anInteger : Int): String = anInteger match {
    case n if n % 15 == 0 => "fizzbuzz"
    case n if n % 3 == 0 => "fizz"
    case n if n % 5 == 0 => "buzz"
    case n => s"$n"
  }
}
